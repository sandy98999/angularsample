import {Component, Input} from "@angular/core"
import { IEvent } from '../shared/event.model';

@Component({
    selector:'event-thumbnail',
    templateUrl: './event-thumbnail.component.html',
    styles:[`
    .thumbnail{
        min-height:210px
    }
    .green{
        color: black;
    }
    bold{
        font-weight: bold;
    }
    `]
})
export class EventThumbnailComponent{

    @Input() event: IEvent;

    timeStyle(){
        const isEarlyStart = this.event && this.event.time === "8:00 am"
        return {green: isEarlyStart, bold: isEarlyStart};
    }
}
