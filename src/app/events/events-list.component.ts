import {Component, OnInit} from "@angular/core"
import { EventService } from '../shared/services';
import { ToasterService } from '../common/toaster.service';
import { ActivatedRoute } from '@angular/router';
import { IEvent } from '../shared/event.model';

@Component({    
    selector:'events-list',
    template: `
    <div>
        <h2>Upcoming Angular Events</h2>
        <hr/>
        <div class="row" >
            <div class="col-md-5" *ngFor="let event of events">
                <event-thumbnail (click)="handleThumnailClick(event?.name)" [event]="event"></event-thumbnail>
            </div>
        </div>
    </div>
    `
})

export class EventsListComponent implements OnInit{
    events: IEvent[]
    constructor(private eventService: EventService, private toastr: ToasterService,
        private route:ActivatedRoute){        
    }
    ngOnInit(){
        this.events = this.route.snapshot.data['events']
    }

    handleThumnailClick(name){
        this.toastr.success(name);
    }
}
